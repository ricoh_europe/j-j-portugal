using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IF_THEN
{
    public class UserModuleClass_IF_THEN : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput IF__DOLLAR__;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> AND__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput ELSE__DOLLAR__;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> THEN__DOLLAR__;
        object IF__DOLLAR___OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort I = 0;
                
                
                __context__.SourceCodeLine = 118;
                _SplusNVRAM.ANDFOUNDPUSH = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 119;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)_SplusNVRAM.LASTAND; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 121;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AND__DOLLAR__[ I ] .Value == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 123;
                        _SplusNVRAM.ANDFOUNDPUSH = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 124;
                        Functions.Pulse ( 10, THEN__DOLLAR__ [ I] ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 119;
                    } 
                
                __context__.SourceCodeLine = 127;
                if ( Functions.TestForTrue  ( ( Functions.Not( _SplusNVRAM.ANDFOUNDPUSH ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 129;
                    Functions.Pulse ( 10, ELSE__DOLLAR__ ) ; 
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    public override object FunctionMain (  object __obj__ ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusFunctionMainStartCode();
            
            __context__.SourceCodeLine = 155;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 20 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)1; 
            int __FN_FORSTEP_VAL__1 = (int)Functions.ToLongInteger( -( 1 ) ); 
            for ( _SplusNVRAM.LASTAND  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (_SplusNVRAM.LASTAND  >= __FN_FORSTART_VAL__1) && (_SplusNVRAM.LASTAND  <= __FN_FOREND_VAL__1) ) : ( (_SplusNVRAM.LASTAND  <= __FN_FORSTART_VAL__1) && (_SplusNVRAM.LASTAND  >= __FN_FOREND_VAL__1) ) ; _SplusNVRAM.LASTAND  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 157;
                if ( Functions.TestForTrue  ( ( IsSignalDefined( AND__DOLLAR__[ _SplusNVRAM.LASTAND ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 159;
                    break ; 
                    } 
                
                __context__.SourceCodeLine = 155;
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        return __obj__;
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        
        IF__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalInput( IF__DOLLAR____DigitalInput__, this );
        m_DigitalInputList.Add( IF__DOLLAR____DigitalInput__, IF__DOLLAR__ );
        
        AND__DOLLAR__ = new InOutArray<DigitalInput>( 20, this );
        for( uint i = 0; i < 20; i++ )
        {
            AND__DOLLAR__[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( AND__DOLLAR____DigitalInput__ + i, AND__DOLLAR____DigitalInput__, this );
            m_DigitalInputList.Add( AND__DOLLAR____DigitalInput__ + i, AND__DOLLAR__[i+1] );
        }
        
        ELSE__DOLLAR__ = new Crestron.Logos.SplusObjects.DigitalOutput( ELSE__DOLLAR____DigitalOutput__, this );
        m_DigitalOutputList.Add( ELSE__DOLLAR____DigitalOutput__, ELSE__DOLLAR__ );
        
        THEN__DOLLAR__ = new InOutArray<DigitalOutput>( 20, this );
        for( uint i = 0; i < 20; i++ )
        {
            THEN__DOLLAR__[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( THEN__DOLLAR____DigitalOutput__ + i, this );
            m_DigitalOutputList.Add( THEN__DOLLAR____DigitalOutput__ + i, THEN__DOLLAR__[i+1] );
        }
        
        
        IF__DOLLAR__.OnDigitalPush.Add( new InputChangeHandlerWrapper( IF__DOLLAR___OnPush_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        
        
    }
    
    public UserModuleClass_IF_THEN ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint IF__DOLLAR____DigitalInput__ = 0;
    const uint AND__DOLLAR____DigitalInput__ = 1;
    const uint ELSE__DOLLAR____DigitalOutput__ = 0;
    const uint THEN__DOLLAR____DigitalOutput__ = 1;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        [SplusStructAttribute(0, false, true)]
            public ushort LASTAND = 0;
            [SplusStructAttribute(1, false, true)]
            public ushort ANDFOUNDPUSH = 0;
            
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
